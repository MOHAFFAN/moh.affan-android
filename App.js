import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

import Navigation from "./navigation/Navigation";
import Screens from "./navigation/Screens";
import Home from "./navigation/Home";
import Banjari from "./navigation/Banjari";

const Affan = createNativeStackNavigator();

function App () {
    return (
        <NavigationContainer>
            <Affan.Navigator initialRouteName="Navigation">
                <Affan.Screen name="Navigation" component={Navigation}/>
                <Affan.Screen name="Home" component={Home}/>
                <Affan.Screen name="Screens" component={Screens}/>
                <Affan.Screen name="Banjari" component={Banjari}/>
            </Affan.Navigator>
        </NavigationContainer>
    )
}

export default App;