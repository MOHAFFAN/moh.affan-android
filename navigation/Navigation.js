import {
    Text,
    View,
    TouchableOpacity,
    Image,
     ImageBackground, TextInput} from 'react-native'
  import React, { Component } from 'react'
  
  const Navigation = ({navigation}) => {
      return (
        <View
        style={{flex: 1}}>
          <View style={{
          }}>
            <View style={{
              backgroundColor: 'yellow',
              width: 470,
              height: 54,
              marginRight: 50,
              marginLeft: 120,
              marginTop: 50,
              borderBottomRightRadius: 45,
              borderBottomLeftRadius: 45,
            }}></View>
            
            <Text style={{
              fontSize: 50,
              fontWeight: 'bold',
              color : '#545454',
              textAlign : 'center',
              marginBottom : 5,
              marginTop : 350,
            }}>LEMBAGA PONDOK PESANTREN ANNUSYUR</Text>
  
          <Text style={{
              fontSize: 25,
              fontWeight: 'bold',
              color : 'black',
              textAlign : 'center',
              marginBottom : 5,
              marginTop : 0.001,
            }}>Aeng panas Sumenep Madura</Text>
  
            <Text style={{
              fontSize: 35,
              fontWeight: 'bold',
              color : 'black',
              textAlign : 'center',
              marginBottom : 5,
              marginTop : 70,
            }}>Silahkan Login</Text>
            </View>
  
        <TextInput
          style={{
            backgroundColor: 'white',
            borderColor: '#000fb1',
            borderWidth: 2,
            width: 470,
            height: 50,
            marginLeft: 120,
            marginRight: 50,
            marginTop: 20,  
            borderRadius: 5,
            fontSize: 25,      
          }}
            placeholder= 'klik START'
        ></TextInput>
  
        <TextInput
          style={{
            backgroundColor: 'white',
            borderColor: '#000fb1',
            borderWidth: 2,
            width: 470,
            height: 50,
            marginLeft: 120,
            marginRight: 50,
            marginTop: 10,
            borderRadius: 5,
            fontSize: 25,
  
          }}
            placeholder= 'Mode otomatis'
        ></TextInput>
  
            <View style={{flexDirection: 'row'}}>
          <TouchableOpacity 
          onPress={() => navigation.navigate('Home')}
          style={{flex : 1,
              backgroundColor : 'black',
              marginTop :20,
              borderRadius : 5,
              marginHorizontal : 50,
              alignItems: 'center',
              marginLeft: 265,
              marginRight: 120,
              width: 20,
              height: 50,

            }}><Text style={{
                color: 'white',
                fontSize: 25,
                marginTop: 7,
                fontWeight: 'bold',
              }}> START</Text>
              </TouchableOpacity>
  
              <TouchableOpacity style={{flex : 1,
            }}></TouchableOpacity>
              </View>
            
            <View style={{
              backgroundColor: 'yellow',
              width: 470,
              height: 700,
              marginRight: 50,
              marginLeft: 120,
              marginTop: 305,
              borderTopRightRadius: 50,
              borderTopLeftRadius: 50,
            }}></View>
              </View>
      );
    }
  
  export default Navigation;