import { Text, View,} from 'react-native'
import React, { Component } from 'react'

export default class Home extends Component {
  render() {
    return (
      <View style={{
        width: 710,
        height: 100,
        backgroundColor: '#ffc901',
        marginLeft: 5,
        borderBottomLeftRadius: 35,
        borderBottomRightRadius: 35, 
      }}>
        <Text style={{
            color: 'white',
            fontSize: 40,
            textAlign: 'center',
            marginTop: 20,
            fontWeight: 'bold'
            }}>HOME</Text>

        <View style={{
        width: 710,
        height: 70,
        backgroundColor: '#0068b0',
        marginLeft: 1,
        borderTopLeftRadius: 35,
        borderTopRightRadius: 35,
        marginTop: 35,
      }}>
        <Text style={{
            color: 'white',
            fontSize: 35,
            textAlign: 'center',
            marginTop: 10,
            fontWeight: 'bold'
            }}>Daftar Lembaga LPP ANNUSYUR</Text>
            </View>

        <View style={{
        elevation: 5,
        borderColor: 'black',
        borderEndColor: 10,
        width: 700,
        height: 60,
        backgroundColor: 'white',
        marginLeft: 6,
        marginTop: 7,
      }}>
        <Text style={{
            color: 'gray',
            fontSize: 30,
            textAlign: 'center',
            marginTop: 6,
            fontWeight: 'bold'
            }}>Pendidikan Anak Usia Dini</Text>
            </View>

    <View style={{
        elevation: 5,
        borderColor: 'black',
        borderEndColor: 10,
        width: 700,
        height: 60,
        backgroundColor: 'white',
        marginLeft: 6,
        marginTop: 7,
      }}>
        <Text style={{
            color: 'gray',
            fontSize: 30,
            textAlign: 'center',
            marginTop: 6,
            fontWeight: 'bold'
            }}>Raudlatul Atfal</Text>
            </View>

    <View style={{
        elevation: 5,
        borderColor: 'black',
        borderEndColor: 10,
        width: 700,
        height: 60,
        backgroundColor: 'white',
        marginLeft: 6,
        marginTop: 7,
      }}>
        <Text style={{
            color: 'gray',
            fontSize: 30,
            textAlign: 'center',
            marginTop: 6,
            fontWeight: 'bold'
            }}>Madrasah Ibtidaiyah</Text>
            </View>
    
    <View style={{
        elevation: 5,
        borderColor: 'black',
        borderEndColor: 10,
        width: 700,
        height: 60,
        backgroundColor: 'white',
        marginLeft: 6,
        marginTop: 7,
      }}>
        <Text style={{
            color: 'gray',
            fontSize: 30,
            textAlign: 'center',
            marginTop: 6,
            fontWeight: 'bold'
            }}>Madrasah Tsanawiyah</Text>
            </View>

    <View style={{
        elevation: 5,
        borderColor: 'black',
        borderEndColor: 10,
        width: 700,
        height: 60,
        backgroundColor: 'white',
        marginLeft: 6,
        marginRight: 7,
        marginTop: 10,
      }}>
        <Text style={{
            color: 'gray',
            fontSize: 30,
            textAlign: 'center',
            marginTop: 6,
            fontWeight: 'bold'
            }}>Madrasah Aliyah</Text>
            </View>

    <View style={{
        borderEndColor: 10,
        width: 710,
        height: 70,
        backgroundColor: '#0068b0',
        marginLeft: 1,
        marginTop: 10,
        borderBottomLeftRadius: 40,
        borderBottomRightRadius: 40,
      }}>
        <Text style={{
            color: 'white',
            fontSize: 30,
            textAlign: 'center',
            marginTop: 12,
            fontWeight: 'bold'
            }}>Silahkan Daftarkan putra/putri kalian</Text>
            </View>

    <View style={{
        backgroundColor: '#0068b0',
        width: 200,
        height: 70,
        marginRight: 30,
        marginLeft: 250,
        marginTop: 20,
        }}>
        </View>

   
      </View>
    )
  }
}